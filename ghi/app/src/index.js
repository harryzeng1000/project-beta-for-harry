import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
async function getData() {
  const resAppointment = await fetch('http://localhost:8080/api/appointments/'); // this should work with any number of endpoints, just add the fetch request, add the variable to the array, and add the corresponding data to the app component

  Promise.all([resAppointment])
    .then(values => Promise.all(values.map(value => value.json())))
    .then(finalVals => {
      let Appointment = finalVals[0];
      root.render(
        <React.StrictMode>
          <App Appointment={Appointment.shoes} />
        </React.StrictMode>
      );
    })
}
getData();
