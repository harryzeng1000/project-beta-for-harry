import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import AppointmentList from "./ServiceComponents/AppointmentList.js";
import TechnicianForm from "./ServiceComponents/TechnicianForm";
import AppointmentForm from "./ServiceComponents/AppointmentForm";
import AppointmentHistory from "./ServiceComponents/AppointmentHistory";

import ManufacturerList from "./Inventory/ManufacturerList";
import ManufacturerForm from "./Inventory/ManufacturerForm";
import AutomobileList from "./Inventory/AutomobileList";
import AutomobileForm from "./Inventory/AutomobileForm";
import VehicleList from "./Inventory/VehicleList";
import VehicleForm from "./Inventory/VehicleForm";


import SalesPersonForm from "./SaleComponents/SalesPersonForm";
import CustomerForm from "./SaleComponents/CustomerForm";
import SaleRecordList from "./SaleComponents/SaleRecordList";
import SaleForm from "./SaleComponents/SaleRecordForm";
import SalePersonSalesList from "./SaleComponents/SalePersonSalesList";

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>

          <Route path="/" element={<MainPage />} />
          <Route path="appointments">
            <Route index element={<AppointmentList />} />
            <Route path="create/" element={<AppointmentForm />} />
            <Route path="history/" element={<AppointmentHistory />} />
          </Route>
          <Route path="technicians">
            <Route index element={<TechnicianForm />} />
          </Route>

          <Route path="sales">
            <Route path="create-seller/" element={<SalesPersonForm />} />
            <Route path="list/" element={<SaleRecordList />} />
            <Route path="create/" element={<SaleForm />} />
            <Route path="seller-list/" element={<SalePersonSalesList />} />
          </Route>
          <Route path="customers" element={<CustomerForm />} />

          <Route path="manufacturers">
            <Route index element={<ManufacturerList />} />
            <Route path="create/" element={<ManufacturerForm />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobileList />} />
            <Route path="create" element={<AutomobileForm />} />
          </Route>
          <Route path="vehicles">
            <Route index element={<VehicleList />} />
            <Route path="create" element={<VehicleForm />} />
          </Route>


        </Routes>
      </div>
    </BrowserRouter >
  );
}

export default App;
