import React, { useState, useEffect } from "react";


function SaleRecordList() {
    const [records, setRecords] = useState([])

    const fetchAllSaleRecords = async () => {
        const res = await fetch(`http://localhost:8090/api/list_sale_records`);
        const saleRecordData = await res.json();
        const arr = saleRecordData['sale_records'];
        setRecords(arr);
    };
    useEffect(() => {
        fetchAllSaleRecords();
    }, []);
    console.log(records);
    return (
        <table className='table table-striped table-hover'>
            <thead>
                <tr>
                    <th>Seller Name</th>
                    <th>Selled ID</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {records.map(record => {
                    return (
                        <tr key={record.id}>
                            <td>{record.sales_person.name}</td>
                            <td>{record.sales_person.id}</td>
                            <td>{record.customer.name}</td>
                            <td>{record.automobile.vin}</td>
                            <td>{record.price}</td>
                        </tr>
                    )
                })}
            </tbody>
        </table >
    )
}
export default SaleRecordList;
