import React, { useState } from "react";

function CustomerForm() {
    const [state, setState] = useState({
        name: '',
        address: '',
        phone: ''
    });
    const [successfulSubmit, setSuccessfulSubmit] = useState(false);

    let formClasses = "";
    let alertClasses = "alert alert-success d-none mb-3";
    let alertContainerClasses = "d-none";

    const handleSubmit = async event => {
        event.preventDefault();
        const data = state
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const res = await fetch("http://localhost:8090/api/list_customer", fetchConfig);

        if (res.ok) {
            setState({
                name: '',
                address: '',
                phone: ''
            });
            setSuccessfulSubmit(true);
        }
    };



    const handleChange = event => {
        const value = event.target.value;
        setState({
            ...state,
            [event.target.name]: value,
        });
    };



    if (successfulSubmit) {
        formClasses = "d-none";
        alertClasses = "alert alert-success d-none mb-3";
        alertContainerClasses = "";
    }

    return (
        <div className='row'>
            <div className='offset-3 col-6'>
                <div className='shadow p-4 mt-4'>
                    <h1>Create New Customer</h1>
                    <form
                        onSubmit={handleSubmit}
                        id='create-customer-form'
                        className={formClasses}>

                        <div className='form-floating mb-3'>
                            <input
                                onChange={handleChange}
                                value={state.name}
                                placeholder='customer name'
                                required
                                name='name'
                                id='customer_name'
                                className='form-control'
                            />
                            <label htmlFor='style_name'>Name</label>
                        </div>

                        <div className='form-floating mb-3'>
                            <input
                                onChange={handleChange}
                                value={state.address}
                                placeholder='customer address'
                                required
                                name='address'
                                id='customer_address'
                                className='form-control'
                            />
                            <label htmlFor='color'>Customer Address</label>
                        </div>

                        <div className='form-floating mb-3'>
                            <input
                                onChange={handleChange}
                                value={state.phone}
                                placeholder='customer phone'
                                required
                                name='phone'
                                id='customer_phone'
                                className='form-control'
                            />
                            <label htmlFor='color'>Customer Phone Number</label>
                        </div>


                        <button className='btn btn-primary'>Create</button>

                    </form>

                    <div className={alertContainerClasses}>
                        <div className={alertClasses} id='success-message'>
                            Customer created successfully
                        </div>
                        <button
                            onClick={() => setSuccessfulSubmit(false)}
                            className='btn btn-primary'
                        >
                            Create another Customer
                        </button>
                    </div>

                </div>
            </div>
        </div>
    )

}
export default CustomerForm;
