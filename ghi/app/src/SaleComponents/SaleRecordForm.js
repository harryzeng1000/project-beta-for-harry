// need to submit the form and delete the corresponding car in inventory

import React, { useEffect, useState } from "react";

function SaleForm() { //may need to use props to get all of the data i need
    const [state, setState] = useState({
        price: '',
        automobile: '',
        sales_person: '',
        customer: ''
    });
    const [automobiles, setAutomobile] = useState([]);
    const [salesPersons, setSalesPerson] = useState([]);
    const [customers, setCustomer] = useState([]);
    const [successfulSubmit, setSuccessfulSubmit] = useState(false);

    let formClasses = "";
    let alertClasses = "alert alert-success d-none mb-3";
    let alertContainerClasses = "d-none";

    useEffect(() => {
        const fetchAutomobileData = async () => {
            const resAutomobile = await fetch('http://localhost:8100/api/automobiles/');
            const automobileData = await resAutomobile.json();
            const automobileArr = automobileData['autos'];
            setAutomobile(automobileArr);
        };
        fetchAutomobileData();


        const fetchSalePersonData = async () => {
            const resSalePerson = await fetch('http://localhost:8090/api/list_sales_persons');
            const salePersonData = await resSalePerson.json();
            //const salePersonArr = salePersonData.
            setSalesPerson(salePersonData);
        }
        fetchSalePersonData();


        const fetchCustomerData = async () => {
            const resCustomer = await fetch('http://localhost:8090/api/list_customer');
            const customerData = await resCustomer.json();
            setCustomer(customerData);
        }
        fetchCustomerData();
    }, [])


    const handleSubmit = async event => {
        event.preventDefault();

        const SaleUrl = "http://localhost:8090/api/list_sale_records"
        const config = {
            method: "POST",
            body: JSON.stringify(state),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(SaleUrl, config);

        if (response.ok) {
            setState({
                price: '',
                automobile: '',
                sales_person: '',
                customer: ''
            });
            setSuccessfulSubmit(true);
        }
    };

    const handleChange = event => {
        const value = event.target.value;
        setState({
            ...state,
            [event.target.name]: value,
        });
    };

    if (successfulSubmit) {
        formClasses = "d-none";
        alertClasses = "alert alert-success mb-3";
        alertContainerClasses = "";
    }

    return (
        <div className='row'>
            <div className='offset-3 col-6'>
                <div className='shadow p-4 mt-4'>
                    <h1>Create a new Sale</h1>
                    <form onSubmit={handleSubmit} id='create-sale-form'
                        className={formClasses}>
                        <div className='mb-3'>
                            <select
                                onChange={handleChange}
                                value={state.automobile}
                                required
                                name='automobile'
                                id='automobile'
                                className='form-select'
                            >

                                <option value=''>Choose a automobile</option>
                                {automobiles.map(automobile => {
                                    return (
                                        <option
                                            key={automobile.vin}
                                            value={automobile.href}
                                        >
                                            {automobile.vin}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>

                        <div className='mb-3'>
                            <select
                                onChange={handleChange}
                                value={state.salesPerson}
                                required
                                name='sales_person'
                                id='sales_person'
                                className='form-select'
                            >

                                <option value=''>Choose a Sales Person</option>
                                {salesPersons.map(salesPerson => {
                                    return (
                                        <option
                                            key={salesPerson.id} // may want to switch this with employee number
                                            value={salesPerson.id}
                                        >
                                            {salesPerson.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>

                        <div className='mb-3'>
                            <select
                                onChange={handleChange}
                                value={state.customer}
                                required
                                name='customer'
                                id='customer'
                                className='form-select'
                            >

                                <option value=''>Choose a Customer</option>
                                {customers.map(customer => {
                                    return (
                                        <option
                                            key={customer.id}
                                            value={customer.id}
                                        >
                                            {customer.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>

                        <div className='form-floating mb-3'>
                            <input
                                onChange={handleChange}
                                value={state.price}
                                placeholder='price'
                                required
                                name='price'
                                id='price'
                                className='form-control'
                            />
                            <label htmlFor='price'>Price</label>
                        </div>

                        <button className='btn btn-primary'>Create</button>
                    </form>

                    <div className={alertContainerClasses}>
                        <div className={alertClasses} id='success-message'>
                            Sale created successfully
                        </div>
                        <button
                            onClick={() => setSuccessfulSubmit(false)}
                            className='btn btn-primary'
                        >
                            Create another Sale
                        </button>
                    </div>

                </div>
            </div>
        </div>
    );
}
export default SaleForm;
