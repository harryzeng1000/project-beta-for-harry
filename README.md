# CarCar

Team:

* Connor Somers - Sales Microservice
* Harry Zeng - Service Microservice

## Design

![design](ghi/app/public/image.png)

## How to Run (simple)
this application uses docker to ensure that it can be build in any environment. First make sure you have docker installed and install the docker desktop.

Once you have the docker desktop, clone and pull the project to your local machine: <br>
    git clone https://gitlab.com/hackreactor-connor/project-beta.git <br>

 from the root of the project, run the following command(s): <br>
    docker-compose up --build

That should be all you need to do to get the app running, but in the event that something goes wrong please refer to the steps below to get a fresh project up and running.

## creating a fresh database and project

### purge the existing database
From the root of the project directory run the following commands: <br>
    docker container prune -f <br>
    docker volume rm project-beta_beta-data <br>

### re-run the project
Again from the root of the project, run: <br>
    docker-compose up --build <br>

### apply migrations
now, for each of the 3 api backends, you will want to initialize the database on each. you will want to use docker desktop to access the terminal for each container and run the following commands in each: <br>
    python manage.py makemigrations <br>
    python manage.py migrate <br>




## Service microservice

There will be a Service model in django. inside the app, there have a customer’s name, car’s info, technicians and fix car reason. There is two bottom for customer or technicians to choose finished or cancel appointments.. The data will be coming from the RESTful API and plugged into the react app. I will also make an AutomobilieVO model that will store the data I need from the Inventory microservice’s Automobile model.

## Sales microservice

In the Sales microservice, there are 4 models. The 'SalesPerson' model which defines and stores the sales persons. There is the 'Customer' model which stores the customers and their data, and finially there is the AutomobileVO which uses a poller and the inventory API to get the automobile data from the inventory microservice to keep track of automobiles. FInially, there is the 'SaleRecord' model which keeps track of sale records with their corresponding automobile, the customer, and the sales persons associated with each sale.



## api Docs
below are the api endpoints for the sales, services, and inventory microservices

### Sales API
http://localhost:8090/api/list_sales_persons <br>
http://localhost:8090/api/list_customer <br>
http://localhost:8090/api/list_sale_records <br>
http://localhost:8090/api/list_sale_records/1 <br>

##### create/list customers
http://localhost:8090/api/list_customer <br>
POST <br>
{ <br>
	"name": "don", <br>
	"address": "874 road TN", <br>
	"phone": 1987654321 <br>
}

##### create/list sales persons
http://localhost:8090/api/list_sales_persons <br>
POST <br>
{ <br>
	"name": "jim", <br>
	"employee_number": 19679 <br>
}

##### create/list sale records
http://localhost:8090/api/list_sale_records <br>
POST <br>
{ <br>
	"price": 123456, <br>
	"automobile": "/api/automobiles/4Y1SL65848Z411438/", <br>
	"sales_person": 4, <br>
	"customer": 2 <br>
}
##### get sale person sale history
http://localhost:8090/api/list_sale_records/1 <br>




### Service API
http://localhost:8080/api/technicians <br>
http://localhost:8080/api/appointments <br>
http://localhost:8080/api/appointments/1 <br>



##### create/list appointments
http://localhost:8080/api/appointments <br>
POST <br>
{ <br>
    "customer_name": "tim", <br>
    "vip": false, <br>
    "vin": "4Y1SL65848Z411438", <br>
    "technician": "3", <br>
    "reason": "fix car", <br>
    "date": "2022-12-21", <br>
    "time": "13:38:00", <br>
    "finished": false <br>
}

##### get appointment
http://localhost:8080/api/appointments/1 <br>



##### create/list technicians
http://localhost:8080/api/technicians <br>
POST <br>
{ <br>
    "name": "tim", <br>
    "employee_number": 12345 <br>
}
