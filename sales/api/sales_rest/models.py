from django.db import models
from django.urls import reverse

# Create your models here.




class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, null=True)
    vin = models.CharField(max_length=300)


class SalesPerson(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.PositiveIntegerField(unique=True)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("sales_person_employee_number", kwargs={'pk': self.id})





class Customer(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=200)
    phone = models.PositiveIntegerField(unique=True)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_customer", kwargs={'pk': self.id})





class SaleRecord(models.Model):
    price = models.PositiveBigIntegerField()

    customer = models.ForeignKey(
        Customer,
        related_name='sale_record',
        on_delete=models.RESTRICT, # not sure if i use restrict or protect here. if a customer is deleted then it should not delete the sale
    )

    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="sale_record",
        on_delete=models.RESTRICT, # not sure if I should restrict this or not. do we keep all sale records?
    )

    automobile = models.OneToOneField(
        AutomobileVO,
        related_name="sale_record",
        on_delete = models.RESTRICT,
        # not sure if i need to add the the pk here. i dont think so, but idk
    )

    def get_api_url(self):
        return reverse("show_sale", kwargs={'pk': self.pk}) # not sure if this is doing anything
