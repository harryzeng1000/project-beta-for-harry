from django.contrib import admin
from django.urls import path

from .api_views import api_list_customers, api_list_sale_records, api_list_sales_persons

urlpatterns= [
    path("list_sale_records", api_list_sale_records),
    path("list_sale_records/<int:sale_person_id>", api_list_sale_records),
    path("list_sales_persons", api_list_sales_persons),
    path("list_customer", api_list_customers),
]
