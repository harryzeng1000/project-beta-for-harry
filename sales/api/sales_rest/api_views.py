from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import AutomobileVO, Customer, SaleRecord, SalesPerson

from .encoders import SalesPersonEncoder, CustomerEncoder, SaleRecordEncoder



@require_http_methods(["GET", "POST"]) # create and list sale record(s)
def api_list_sale_records(request, sale_person_id=None):
    if request.method == "GET":
        if sale_person_id is not None:
            sale_records = SaleRecord.objects.filter(sales_person = sale_person_id)
        else:
            sale_records = SaleRecord.objects.all()
        return JsonResponse(
            {'sale_records': sale_records},
            encoder = SaleRecordEncoder
        )
    else:
        content = json.loads(request.body)

        try:
            automobile = AutomobileVO.objects.get(import_href=content['automobile'])
            content['automobile'] = automobile

            customer = Customer.objects.get(id=content['customer'])
            content['customer'] = customer

            sales_person = SalesPerson.objects.get(id=content['sales_person'])
            content['sales_person'] = sales_person
            # sometimes need to wait for poller to get the updated inventory

        except AutomobileVO.DoesNotExist:
             return JsonResponse(
                {'message': 'car does not exist'},
                status=400,
            )
        sale_record = SaleRecord.objects.create(**content)
        return JsonResponse(
            sale_record,
            encoder = SaleRecordEncoder,
            safe=False,
        )



@require_http_methods(["GET","POST"])
def api_list_customers(request): # not sure if this is right
    if request.method == "POST":
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder = CustomerEncoder,
            safe=False,
        )
    else:
        customers = Customer.objects.all()
        return JsonResponse(
            customers,
            encoder = CustomerEncoder,
            safe=False
        )

@require_http_methods(["GET","POST"])
def api_list_sales_persons(request):
    if request.method == "POST":
        content = json.loads(request.body)
        sales_person = SalesPerson.objects.create(**content)
        return JsonResponse(
            sales_person,
            encoder = SalesPersonEncoder,
            safe=False
        )
    else: # GET
        sales_persons = SalesPerson.objects.all()
        return JsonResponse(
            sales_persons,
            encoder = SalesPersonEncoder,
            safe=False
        )
