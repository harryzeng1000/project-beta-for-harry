from common.json import ModelEncoder

from .models import AutomobileVO, Customer, SaleRecord, SalesPerson
# Create your views here.


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        'import_href',
        'vin',
        'id',
    ]

class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        'name',
        'employee_number',
        'id',
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        'name',
        'address',
        'phone',
        'id',
    ]


class SaleRecordEncoder(ModelEncoder):
    model=SaleRecord
    properties = [
        'price',
        'customer',
        'sales_person',
        'automobile',
        'id'
    ]

    encoders = {
        "automobile": AutomobileVOEncoder(),
        'customer': CustomerEncoder(),
        'sales_person': SalesPersonEncoder(),
    }
